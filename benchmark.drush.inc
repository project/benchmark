<?php

/**
 * @file
 * Benchmark module drush integration.
 */

define('BENCHMARK_DEFAULT_TITLE', dt('Benchmark'));

/**
 * Implements hook_drush_command().
 */
function benchmark_drush_command() {

  $items['benchmark-run'] = array(
    'description' => 'Run benchmark',
    'examples' => array(
      'drush br --uri=http://example.com/ --group=Actions --title=\'New benchmark\'' => 'Execute benchmark with Actions test group.',
      'drush br --uri=http://example.com/' => 'Execute benchmark with default test group.',
      'drush br --uri=http://example.com/ --bid=50' => 'Execute benchmark from existing Benchmark entity.',
    ),
    'options' => array(
      'group' => 'A test group. If omitted, default group will be used.',
      'title' => 'Benchmark title. If omitted, default title will be used.',
      'bid' => 'Benchmark ID. If omitted, new benchmark entity will be created.',
      'no-save' => 'Do not save test results.',
      'display-errors' => 'Display test fails and exceptions.',
      'directory' => 'Benchmark directory relative to the public file system path.',
    ),
    'drupal dependencies' => array('simpletest', 'benchmark'),
    'bootstrap' => DRUSH_BOOTSTRAP_DRUPAL_FULL,
    'aliases' => array('br'),
  );

  return $items;
}

/**
 * Implements drush_validate().
 */
function drush_benchmark_run_validate() {
  if (!drush_get_option('uri')) {
    return drush_set_error(dt('You must specify this site\'s URL using the --uri parameter.'));
  }
}

/**
 * Test-run command callback.
 *
 * @test_group
 *   A group name.
 */
function drush_benchmark_run() {

  if (drush_get_option('bid')) {
    $benchmark  = benchmark_load(drush_get_option('bid'));
    if (!$benchmark) {
      drush_set_error('BENCHMARK_FAIL', dt('Could not load benchmark entity.'));
      return;
    }
    $benchmark->title = drush_get_option('title', $benchmark->title);
    $benchmark->test_group = drush_get_option('group', $benchmark->group);
  }
  else {
    $benchmark = entity_get_controller('benchmark')->create();
    $benchmark->title = drush_get_option('title', BENCHMARK_DEFAULT_TITLE);
    $benchmark->test_group = drush_get_option('group', variable_get('benchmark_default_group'));
  }

  cache_clear_all('simpletest', 'cache');
  module_load_include('inc', 'benchmark', 'benchmark.admin');
  $groups = simpletest_test_get_all();

  if (empty($groups[$benchmark->test_group])) {
    return drush_set_error('BENCHMARK_FAIL', dt('Could not find test group.'));
  }

  // Directory option allows avaid file permission issues.
  if ($directory = drush_get_option('directory')) {
    global $conf;
    $conf['file_public_path'] = $conf['file_public_path'] . '/' . $directory;
  }

  $counter = 1;
  $total = count($groups[$benchmark->test_group]);
  $start_time = time();
  foreach ($groups[$benchmark->test_group] as $class_name => $test_case) {

    $tests_list[] = $class_name;
    if (benchmark_drush_run_test($class_name)) {
      $result = time() - $start_time;
      $message = dt('!elapsed (!counter from !total)', array(
        '!counter' => $counter,
        '!total' => $total,
        '!elapsed' => format_interval($result),
      ));
      drush_log($message, 'status');
    }
    else {
      drush_set_error('BENCHMARK_FAIL', dt('Could not proceed benchmark due the test fail.'));
      return;
    }
    $counter++;

  }

  simpletest_clean_environment();

  // Hide messages from simpletest_clean_environment().
  drupal_get_messages();

  drush_print(str_repeat('-', 55));
  drush_print(dt('Total execution time: !elapsed.', array('!elapsed' => format_interval(time() - $start_time))));

  if (!drush_get_option('no-save')) {
    $benchmark->result = $result;
    $benchmark->executed = TRUE;
    $benchmark = benchmark_save($benchmark);
    drush_log(dt('Benchmark URL: !url', array('!url' => drush_get_option('uri') . 'benchmark/' . $benchmark->bid)), 'status');
  }

}

/**
 * Run a single test.
 */
function benchmark_drush_run_test($class) {
  $test_id = db_insert('simpletest_test_id')
    ->useDefaults(array('test_id'))
    ->execute();
  $test = new $class($test_id);
  $test->run();

  if (!$test->results['#pass'] || $test->results['#fail'] || $test->results['#exception']) {
    module_load_include('inc', 'simpletest', 'simpletest.pages');

    if (drush_get_option('display-errors')) {
      $results = simpletest_result_get($test_id);
      $assertions = array_shift($results);
      foreach ($assertions as $assertion) {
        if ($assertion->status == 'fail' || $assertion->status == 'exception') {
          print_r($assertion);
        }
      }
    }

    $summary = _simpletest_format_summary_line($test->results);
    drush_set_error('BENCHMARK_FAIL', dt('!class failed: !summary.', array('!class' => $class, '!summary' => $summary)));
    return FALSE;
  }
  return $test_id;
}
