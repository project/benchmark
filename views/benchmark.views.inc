<?php

/**
 * @file
 * Provide views data.
 */

/**
 * Implements hook_views_data().
 */
function benchmark_views_data() {

  // Define the base group of this table. Fields that don't
  // have a group defined will go into this field by default.
  $data['benchmark']['table']['group'] = t('Benchmark');

  // Advertise this table as a possible base table.
  $data['benchmark']['table']['base'] = array(
    'field' => 'bid',
    'title' => t('Benchmarks'),
    'weight' => -10,
    'access query tag' => 'benchmark_access',
    'defaults' => array(
      'field' => 'title',
    ),
  );
  $data['benchmark']['table']['entity type'] = 'benchmark';

  // Bid.
  $data['benchmark']['bid'] = array(
    'title' => t('Bid'),
    'help' => t('The benchmark ID.'),
    'field' => array(
      'handler' => 'views_handler_field_numeric',
      'click sortable' => TRUE,
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_numeric',
      'name field' => 'title',
      'numeric' => TRUE,
      'validate type' => 'nid',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );

  // Title.
  $data['benchmark']['title'] = array(
    'title' => t('Title'),
    'help' => t('The benchmark title.'),
    'field' => array(
      'field' => 'title',
      'group' => t('Benchmarks'),
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
      'link_to_benchmark default' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_string',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_string',
    ),
  );

  // Test group.
  $data['benchmark']['test_group'] = array(
    'title' => t('Test group'),
    'help' => t('The benchmark test group.'),
    'field' => array(
      'field' => 'test_group',
      'group' => t('Benchmarks'),
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
      'link_to_benchmark default' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_string',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_string',
    ),
  );

  // Result.
  $data['benchmark']['result'] = array(
    'title' => t('Result'),
    'help' => t('The benchmark result.'),
    'field' => array(
      'handler' => 'views_handler_field_time_interval',
      'click sortable' => TRUE,
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_numeric',
      'name field' => 'title',
      'numeric' => TRUE,
      'validate type' => 'nid',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );

  // Created.
  $data['benchmark']['created'] = array(
    'title' => t('Post date'),
    'help' => t('The date the benchmark was posted.'),
    'field' => array(
      'handler' => 'views_handler_field_date',
      'click sortable' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort_date',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_date',
    ),
  );

  // Executed.
  $data['benchmark']['executed'] = array(
    'title' => t('Executed date'),
    'help' => t('The date the benchmark was executed.'),
    'field' => array(
      'handler' => 'views_handler_field_date',
      'click sortable' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort_date',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_date',
    ),
  );

  // System information.
  $data['benchmark']['system_info'] = array(
    'title' => t('System information'),
    'help' => t('The benchmark system information.'),
    'field' => array(
      'field' => 'system_info',
      'group' => t('Benchmarks'),
      'handler' => 'views_handler_field_serialized',
      'click sortable' => TRUE,
      'link_to_benchmark default' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_string',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_string',
    ),
  );

  return $data;
}
