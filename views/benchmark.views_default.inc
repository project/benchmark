<?php

/**
 * @file
 * Provide a default view.
 */

/**
 * Implements hook_views_default_views().
 */
function benchmark_views_default_views() {

  $view = new view();
  $view->name = 'benchmarks';
  $view->description = 'A list of all benchmarks.';
  $view->tag = 'default';
  $view->base_table = 'benchmark';
  $view->human_name = 'Benchmarks';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['title'] = 'Benchmarks';
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'none';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'full';
  $handler->display->display_options['pager']['options']['items_per_page'] = '20';
  $handler->display->display_options['pager']['options']['offset'] = '0';
  $handler->display->display_options['pager']['options']['id'] = '0';
  $handler->display->display_options['pager']['options']['quantity'] = '9';
  $handler->display->display_options['pager']['options']['expose']['items_per_page'] = TRUE;
  $handler->display->display_options['pager']['options']['expose']['items_per_page_options'] = '10, 20,  50';
  $handler->display->display_options['pager']['options']['expose']['items_per_page_options_all'] = TRUE;
  $handler->display->display_options['style_plugin'] = 'table';
  $handler->display->display_options['style_options']['default_row_class'] = FALSE;
  $handler->display->display_options['style_options']['columns'] = array(
    'bid' => 'bid',
    'title' => 'title',
    'result' => 'result',
    'executed' => 'executed',
    'test_group' => 'test_group',
  );
  $handler->display->display_options['style_options']['default'] = 'executed';
  $handler->display->display_options['style_options']['info'] = array(
    'bid' => array(
      'sortable' => 1,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'title_1' => array(
      'sortable' => 1,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'result' => array(
      'sortable' => 1,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'executed' => array(
      'sortable' => 1,
      'default_sort_order' => 'desc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'test_group' => array(
      'sortable' => 1,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
  );
  /* Field: Benchmark ID */
  $handler->display->display_options['fields']['bid']['id'] = 'bid';
  $handler->display->display_options['fields']['bid']['table'] = 'benchmark';
  $handler->display->display_options['fields']['bid']['field'] = 'bid';
  $handler->display->display_options['fields']['bid']['ui_name'] = 'Benchmark ID';
  $handler->display->display_options['fields']['bid']['label'] = 'ID';
  $handler->display->display_options['fields']['bid']['element_default_classes'] = FALSE;
  $handler->display->display_options['fields']['bid']['separator'] = '';
  /* Field: Title */
  $handler->display->display_options['fields']['title_1']['id'] = 'title_1';
  $handler->display->display_options['fields']['title_1']['table'] = 'benchmark';
  $handler->display->display_options['fields']['title_1']['field'] = 'title';
  $handler->display->display_options['fields']['title_1']['ui_name'] = 'Title';
  $handler->display->display_options['fields']['title_1']['element_default_classes'] = FALSE;
  /* Field: Result */
  $handler->display->display_options['fields']['result']['id'] = 'result';
  $handler->display->display_options['fields']['result']['table'] = 'benchmark';
  $handler->display->display_options['fields']['result']['field'] = 'result';
  $handler->display->display_options['fields']['result']['ui_name'] = 'Result';
  $handler->display->display_options['fields']['result']['element_default_classes'] = FALSE;
  $handler->display->display_options['fields']['result']['granularity'] = '2';
  /* Field: Test group */
  $handler->display->display_options['fields']['test_group']['id'] = 'test_group';
  $handler->display->display_options['fields']['test_group']['table'] = 'benchmark';
  $handler->display->display_options['fields']['test_group']['field'] = 'test_group';
  $handler->display->display_options['fields']['test_group']['ui_name'] = 'Test group';
  $handler->display->display_options['fields']['test_group']['element_default_classes'] = FALSE;
  /* Field: Executed date */
  $handler->display->display_options['fields']['executed']['id'] = 'executed';
  $handler->display->display_options['fields']['executed']['table'] = 'benchmark';
  $handler->display->display_options['fields']['executed']['field'] = 'executed';
  $handler->display->display_options['fields']['executed']['ui_name'] = 'Executed date';
  $handler->display->display_options['fields']['executed']['label'] = 'Executed';
  $handler->display->display_options['fields']['executed']['element_default_classes'] = FALSE;
  $handler->display->display_options['fields']['executed']['date_format'] = 'short';
  /* Filter criterion: Title */
  $handler->display->display_options['filters']['title']['id'] = 'title';
  $handler->display->display_options['filters']['title']['table'] = 'benchmark';
  $handler->display->display_options['filters']['title']['field'] = 'title';
  $handler->display->display_options['filters']['title']['ui_name'] = 'Title';
  $handler->display->display_options['filters']['title']['operator'] = 'contains';
  $handler->display->display_options['filters']['title']['group'] = 1;
  $handler->display->display_options['filters']['title']['exposed'] = TRUE;
  $handler->display->display_options['filters']['title']['expose']['operator_id'] = 'title_op';
  $handler->display->display_options['filters']['title']['expose']['label'] = 'Title';
  $handler->display->display_options['filters']['title']['expose']['operator'] = 'title_op';
  $handler->display->display_options['filters']['title']['expose']['identifier'] = 'title';
  $handler->display->display_options['filters']['title']['expose']['remember_roles'] = array(
    2 => '2',
    1 => 0,
    3 => 0,
  );
  /* Filter criterion: Test group */
  $handler->display->display_options['filters']['test_group']['id'] = 'test_group';
  $handler->display->display_options['filters']['test_group']['table'] = 'benchmark';
  $handler->display->display_options['filters']['test_group']['field'] = 'test_group';
  $handler->display->display_options['filters']['test_group']['ui_name'] = 'Test group';
  $handler->display->display_options['filters']['test_group']['operator'] = 'contains';
  $handler->display->display_options['filters']['test_group']['group'] = 1;
  $handler->display->display_options['filters']['test_group']['exposed'] = TRUE;
  $handler->display->display_options['filters']['test_group']['expose']['operator_id'] = 'test_group_op';
  $handler->display->display_options['filters']['test_group']['expose']['label'] = 'Test group';
  $handler->display->display_options['filters']['test_group']['expose']['operator'] = 'test_group_op';
  $handler->display->display_options['filters']['test_group']['expose']['identifier'] = 'test_group';
  $handler->display->display_options['filters']['test_group']['expose']['remember_roles'] = array(
    2 => '2',
    1 => 0,
    3 => 0,
  );

  /* Display: Page */
  $handler = $view->new_display('page', 'Page', 'page');
  $handler->display->display_options['path'] = 'benchmarks';
  $handler->display->display_options['menu']['type'] = 'normal';
  $handler->display->display_options['menu']['title'] = 'Benchmarks';
  $handler->display->display_options['menu']['weight'] = '0';
  $handler->display->display_options['menu']['context'] = 0;
  $translatables['benchmarks'] = array(
    t('Master'),
    t('Benchmarks'),
    t('more'),
    t('Apply'),
    t('Reset'),
    t('Sort by'),
    t('Asc'),
    t('Desc'),
    t('Items per page'),
    t('- All -'),
    t('Offset'),
    t('« first'),
    t('‹ previous'),
    t('next ›'),
    t('last »'),
    t('ID'),
    t('.'),
    t('Title'),
    t('Result'),
    t('Test group'),
    t('Executed'),
    t('Page'),
  );

  $views[$view->name] = $view;
  return $views;
}
