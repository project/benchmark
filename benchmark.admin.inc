<?php

/**
 * @file
 * Benchmark administration.
 */

/**
 * Callback for a page title when this entity is displayed.
 */
function benchmark_title($benchmark) {
  return check_plain($benchmark->title);
}

/**
 * Menu callback to display a benchmark.
 */
function benchmark_view($benchmark, $view_mode = 'default') {

  benchmark_set_breadcrumb();

  // Our entity type, for convenience.
  $entity_type = 'benchmark';

  $benchmark->content = array(
    '#view_mode' => $view_mode,
  );

  field_attach_prepare_view($entity_type, array($benchmark->bid => $benchmark), $view_mode);
  entity_prepare_view($entity_type, array($benchmark->bid => $benchmark));
  $benchmark->content += field_attach_view($entity_type, $benchmark, $view_mode);

  if ($benchmark->result) {
    $benchmark->content['info'] = array(
      '#type' => 'fieldset',
      '#title' => t('Benchmark information'),
    );
    $rows[] = array(t('Result'), format_interval($benchmark->result));
    $rows[] = array(t('Test group'), $benchmark->test_group);
    $rows[] = array(t('Date'), format_date($benchmark->executed));

    // Attach system information.
    foreach ($benchmark->system_info as $key => $value) {
      $rows[] = array($key, $value);
    }
    $benchmark->content['info']['table'] = array(
      '#theme' => 'table',
      '#rows' => $rows,
    );
  }
  module_invoke_all('entity_view', $benchmark, $entity_type, $view_mode, $GLOBALS['language']->language);
  drupal_alter(array('benchmark_view', 'entity_view'), $benchmark->content, $entity_type);

  return $benchmark->content;
}

/**
 * Set benchmark breadcrumb.
 */
function benchmark_set_breadcrumb() {
  if (user_access('access administration pages') && user_access('administer benchmark')) {
    $breadcrumb = array(
      l(t('Home'), '<front>'),
      l(t('Administration'), 'admin'),
      l(t('Structure'), 'admin/structure'),
      l(t('Benchmarks'), 'admin/structure/benchmark'),
    );
    drupal_set_breadcrumb($breadcrumb);
  }
}

/**
 * Form function to create an benchmark entity.
 *
 * The pattern is:
 * - Set up the form for the data that is specific to your
 *   entity: the columns of your base table.
 * - Call on the Field API to pull in the form elements
 *   for fields attached to the entity.
 */
function benchmark_form($form, &$form_state, $benchmark = FALSE) {

  benchmark_set_breadcrumb();

  // New benchmark.
  if (!$benchmark) {
    $benchmark = entity_get_controller('benchmark')->create();
  }

  field_attach_form('benchmark', $benchmark, $form, $form_state);

  $form['title'] = array(
    '#type' => 'textfield',
    '#title' => t('Title'),
    '#required' => TRUE,
    '#size' => 30,
    '#default_value' => $benchmark->title,
    '#weight' => -10,
  );

  $form['test_group']['execute'] = array(
    '#type' => 'checkbox',
    '#title' => t('Execute'),
    '#default_value' => TRUE,
  );

  $form['test_group']['test_group'] = array(
    '#type' => 'select',
    '#title' => t('Test group'),
    '#options' => benchmark_test_groups_options(),
    '#default_value' => $benchmark->test_group ? $benchmark->test_group : variable_get('benchmark_default_group'),
    '#required' => TRUE,
    '#states' => array(
      'visible' => array(
        ':input[name="execute"]' => array('checked' => TRUE),
      ),
    ),
  );

  $form['benchmark'] = array(
    '#type' => 'value',
    '#value' => $benchmark,
  );

  $form['actions']['#weight'] = 10;
  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
  );
  $form['actions']['delete'] = array(
    '#type' => 'submit',
    '#value' => t('Delete'),
    '#submit' => array('benchmark_delete_submit'),
  );

  return $form;
}

/**
 * Validation handler for benchmark form.
 *
 * We pass things straight through to the Field API to handle validation
 * of the attached fields.
 */
function benchmark_form_validate($form, &$form_state) {
  field_attach_form_validate('benchmark', $form_state['values']['benchmark'], $form, $form_state);
}

/**
 * Form submit handler: submits benchmark form information.
 */
function benchmark_form_submit($form, &$form_state) {

  $benchmark = $form_state['values']['benchmark'];
  $benchmark->title = $form_state['values']['title'];
  field_attach_submit('benchmark', $benchmark, $form, $form_state);

  if ($form_state['values']['execute']) {
    $benchmark->test_group = $form_state['values']['test_group'];
    $groups = simpletest_test_get_all();

    foreach ($groups[$benchmark->test_group] as $class_name => $test_case) {
      $tests_list[] = $class_name;
    }
    $test_id = benchmark_run_tests($tests_list);

    $_SESSION['benchmarks'][$test_id]['benchmark'] = $benchmark;
    $_SESSION['benchmarks'][$test_id]['start'] = time();

  }
  else {
    $benchmark = benchmark_save($benchmark);
    $form_state['redirect'] = 'benchmark/' . $benchmark->bid;
  }
}

/**
 * Form deletion handler.
 */
function benchmark_delete_submit($form, &$form_state) {
  $destination = array();
  if (isset($_GET['destination'])) {
    $destination = drupal_get_destination();
    unset($_GET['destination']);
  }
  $benchmark = $form_state['values']['benchmark'];
  $form_state['redirect'] = array('benchmark/' . $benchmark->bid . '/delete', array('query' => $destination));
}

/**
 * Menu callback - ask for confirmation of benchmark deletion.
 */
function benchmark_delete_confirm($form, &$form_state, $benchmark) {
  benchmark_set_breadcrumb();
  $form['#benchmark'] = $benchmark;
  // Always provide entity id in the same form key as in the entity edit form.
  $form['bid'] = array(
    '#type' => 'value',
    '#value' => $benchmark->bid,
  );
  return confirm_form($form,
    t('Are you sure you want to delete  benchmark «%title»?', array('%title' => $benchmark->title)),
    'benchmark/' . $benchmark->bid,
    t('This action cannot be undone.'),
    t('Delete'),
    t('Cancel')
  );
}

/**
 * Execute benchmark deletation.
 */
function benchmark_delete_confirm_submit($form, &$form_state) {
  if ($form_state['values']['confirm']) {
    $benchmark = benchmark_load($form_state['values']['bid']);
    benchmark_delete($benchmark);
    watchdog('benchmark', 'benchmark «%title». has been deleted.', array('%title' => $benchmark->title));
    drupal_set_message(t('benchmark «%title». has been deleted', array('%title' => $benchmark->title)));
  }
  $form_state['redirect'] = 'admin/structure/benchmark';
}

/**
 * Actually runs tests.
 *
 * @param array $test_list
 *   List of tests to run.
 */
function benchmark_run_tests($test_list) {
  $test_id = db_insert('simpletest_test_id')
    ->useDefaults(array('test_id'))
    ->execute();

  // Clear out the previous verbose files.
  file_unmanaged_delete_recursive('public://simpletest/verbose');

  // Get the info for the first test being run.
  $first_test = array_shift($test_list);
  $first_instance = new $first_test();
  array_unshift($test_list, $first_test);
  $info = $first_instance->getInfo();

  $batch = array(
    'title' => t('Running benchmark'),
    'operations' => array(
      array('_benchmark_batch_operation', array($test_list, $test_id)),
    ),
    'finished' => '_benchmark_batch_finished',
    'progress_message' => 'Elapsed time: @elapsed ',
    'file' => drupal_get_path('module', 'benchmark') . '/benchmark.admin.inc',
  );
  batch_set($batch);
  module_invoke_all('test_group_started');

  return $test_id;
}


/**
 * Batch operation callback.
 */
function _benchmark_batch_operation($test_list_init, $test_id, &$context) {

  // Get working values.
  if (!isset($context['sandbox']['max'])) {
    // First iteration: initialize working values.
    $test_list = $test_list_init;
    $context['sandbox']['max'] = count($test_list);
    $test_results = array(
      '#pass' => 0,
      '#fail' => 0,
      '#exception' => 0,
      '#debug' => 0,
    );
  }
  else {
    // Nth iteration: get the current values where we last stored them.
    $test_list = $context['sandbox']['tests'];
    $test_results = $context['sandbox']['test_results'];
  }
  $max = $context['sandbox']['max'];

  // Perform the next test.
  $test_class = array_shift($test_list);
  $test = new $test_class($test_id);
  $test->run();
  $size = count($test_list);
  $info = $test->getInfo();

  module_invoke_all('test_finished', $test->results);

  // Gather results and compose the report.
  $test_results[$test_class] = $test->results;
  foreach ($test_results[$test_class] as $key => $value) {
    $test_results[$key] += $value;
  }
  $test_results[$test_class]['#name'] = $info['name'];

  // Save working values for the next iteration.
  $context['sandbox']['tests'] = $test_list;
  $context['sandbox']['test_results'] = $test_results;
  // The test_id is the only thing we need to save for the report page.
  $context['results']['test_id'] = $test_id;

  // Multistep processing: report progress.
  $context['finished'] = 1 - $size / $max;

}

/**
 * Menu callback: Reports the status of batch operation.
 *
 * @param bool $success
 *   A boolean indicating whether the batch mass update operation successfully
 *   concluded.
 * @param int $results
 *   The number of nodes updated via the batch mode process.
 * @param array $operations
 *   An array of function calls (not used in this function).
 */
function _benchmark_batch_finished($success, $results, $operations, $elapsed) {
  if ($success) {
    $benchmark = $_SESSION['benchmarks'][$results['test_id']]['benchmark'];
    $benchmark->result = time() - $_SESSION['benchmarks'][$results['test_id']]['start'];
    $benchmark->executed = TRUE;
    $benchmark = benchmark_save($benchmark);
    drupal_set_message(t('The benchmark result is: @elapsed.', array('@elapsed' => format_interval($benchmark->result))));
    drupal_goto('benchmark/' . $benchmark->bid);
  }
  else {
    drupal_set_message('Benchmark failed.', 'error');
    drupal_goto('admin/structure/benchmark');
  }

}

/**
 * Benchmarks overview.
 */
function benchmark_overview_list() {

  $header = array(
    array('data' => t('ID'), 'field' => 'bid'),
    array('data' => t('Title'), 'field' => 'title'),
    array('data' => t('Test group'), 'field' => 'test_group'),
    array('data' => t('Result'), 'field' => 'result'),
    array('data' => t('Executed'), 'field' => 'executed', 'sort' => 'desc'),
    array('data' => t('Actions')),
  );

  $query = db_select('benchmark', 'b')
    ->fields('b')
    ->extend('PagerDefault')
    ->extend('TableSort')
    ->limit(20)
    ->orderByHeader($header);
  $result = $query->execute();

  $rows = array();
  foreach ($result as $row) {
    $actions = array(
      l(t('Edit'), 'benchmark/' . $row->bid . '/edit'),
      l(t('Delete'), 'benchmark/' . $row->bid . '/delete'),
    );
    $rows[] = array(
      $row->bid,
      l(check_plain($row->title), 'benchmark/' . $row->bid),
      $row->test_group,
      $row->result ? format_interval($row->result) : '',
      $row->executed ? format_date($row->executed) : '',
      implode('<br/>', $actions),
    );
  }

  $build['table'] = array(
    '#theme' => 'table',
    '#header' => $header,
    '#rows' => $rows,
    '#empty' => t('Sorry, no benchmarks found.'),
  );
  $build['pager'] = array(
    '#theme' => 'pager',
  );
  return $build;
}

/**
 * Benchmark settings form.
 */
function benchmark_settings_form($form, &$form_state) {

  $form['benchmark_default_group'] = array(
    '#type' => 'select',
    '#title' => t('Default test group'),
    '#options' => benchmark_test_groups_options(),
    '#default_value' => variable_get('benchmark_default_group'),
  );

  return system_settings_form($form);
}

/**
 * Generate the list of test groups.
 */
function benchmark_test_groups_options() {
  $groups = array_keys(simpletest_test_get_all());
  return drupal_map_assoc($groups);
}
