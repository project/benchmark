<?php

/**
 * @file
 * Provides a controller building upon the core controller.
 */

/**
 * BenchmarkController extends DrupalDefaultEntityController.
 *
 * Our subclass of DrupalDefaultEntityController lets us add a few
 * important create, update, and delete methods.
 */
class BenchmarkController extends DrupalDefaultEntityController {

  /**
   * Create and return a new benchmark entity.
   */
  public function create() {
    $entity = new stdClass();
    $entity->title = '';
    $entity->type = 'benchmark';
    $entity->bundle_type = 'benchmark';
    $entity->test_group = '';
    $entity->system_info = array();
    $entity->result = NULL;
    return $entity;
  }

  /**
   * Saves the custom fields using drupal_write_record().
   */
  public function save($entity) {
    // If our entity has no bid, then we need to give it a
    // time of creation.
    if (empty($entity->bid)) {
      $is_new = TRUE;
      $entity->created = REQUEST_TIME;
    }

    if (!empty($entity->executed)) {
      $entity->executed = REQUEST_TIME;
      $info['PHP version'] = PHP_VERSION;
      $class = 'DatabaseTasks_' . Database::getConnection()->driver();
      $tasks = new $class();
      $info['Database system'] = $tasks->name();
      $info['Drupal version'] = VERSION;

      $module_info = system_get_info('module', 'benchmark');
      $info['Benchmark version'] = $module_info['version'];

      // Detect opcode cache.
      if (function_exists('apc_add')) {
        $info['Opcode cache'] = 'APC';
      }
      elseif (function_exists('xcache_isset')) {
        $info['Opcode cache'] = 'Xcache';
      }
      elseif (ini_get('eaccelerator.enable')) {
        $info['Opcode cache'] = 'eAccelerator';
      }
      else {
        $info['Opcode cache'] = '';
      }
      $entity->system_info = $info;
    }
    $entity->system_info = serialize($entity->system_info);

    // Invoke hook_entity_presave().
    module_invoke_all('entity_presave', 'benchmark', $entity);
    // The 'primary_keys' argument determines whether this will be an insert
    // or an update. So if the entity already has an ID, we'll specify
    // bid as the key.
    $primary_keys = isset($entity->bid) ? 'bid' : array();
    // Write out the entity record.
    drupal_write_record('benchmark', $entity, $primary_keys);
    // We're going to invoke either hook_entity_update() or
    // hook_entity_insert(), depending on whether or not this is a
    // new entity. We'll just store the name of hook_entity_insert()
    // and change it if we need to.
    $invocation = 'entity_insert';
    // Now we need to either insert or update the fields which are
    // attached to this entity. We use the same primary_keys logic
    // to determine whether to update or insert, and which hook we
    // need to invoke.
    if (empty($primary_keys)) {
      field_attach_insert('benchmark', $entity);
    }
    else {
      field_attach_update('benchmark', $entity);
      $invocation = 'entity_update';
    }
    // Invoke either hook_entity_update() or hook_entity_insert().
    module_invoke_all($invocation, 'benchmark', $entity);

    $message = empty($is_new) ? 'Benchmark "!title" has been updated.' : 'Benchmark "!title" has been created.';
    watchdog('benchmark', $message, array('!title' => $entity->title), WATCHDOG_NOTICE, l(t('View'), 'benchmark/' . $entity->bid));
    return $entity;
  }

  /**
   * Load benchmark entities.
   */
  public function load($ids = array(), $conditions = array()) {
    $entities = parent::load($ids, $conditions);
    foreach ($entities as $entity) {
      if (is_string($entity->system_info)) {
        $entity->system_info = unserialize($entity->system_info);
      }
    }
    return $entities;
  }

  /**
   * Delete a single entity.
   *
   * Really a convenience function for delete_multiple().
   */
  public function delete($entity) {
    $this->deleteMultiple(array($entity));
  }

  /**
   * Delete one or more benchmark entities.
   *
   * Deletion is unfortunately not supported in the base
   * DrupalDefaultEntityController class.
   */
  public function deleteMultiple($entities) {
    $bids = array();
    if (!empty($entities)) {
      $transaction = db_transaction();
      try {
        foreach ($entities as $entity) {
          module_invoke_all('benchmark_delete', $entity);
          // Invoke hook_entity_delete().
          module_invoke_all('entity_delete', $entity, 'benchmark');
          field_attach_delete('benchmark', $entity);
          $bids[] = $entity->bid;
        }
        db_delete('benchmark')
          ->condition('bid', $bids, 'IN')
          ->execute();
      }
      catch (Exception $e) {
        $transaction->rollback();
        watchdog_exception('benchmark', $e);
        throw $e;
      }
    }
  }
}
